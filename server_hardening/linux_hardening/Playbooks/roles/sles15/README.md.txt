# sles15

Introduction
------------
This ansible role applies EON Server Hardening guidelines inline with the CIS_SUSE_Linux_Enterprise_15_Benchmark_v1.1.0. 

This role was developed and tested against SUSE Linux Enterprise Server 15 (SP3) with equal success.


Why Would I Use This Role?
--------------------------
If you are attempting to obtain compliance against an industry-accepted security standard, like PCI DSS, APRA or ISO 27001, then you need to demonstrate that you have applied documented hardening standards against all systems within scope of assessment.

If you are running SUSE Linux Enterprise Server 15, then this role attempts to provide one piece of the solution to the compliance puzzle.


Here Be Dragons!
----------------
If you are considering applying this role to any servers, you should have a basic familiarity with the CIS Benchmark (or other similar benchmarks) and an appreciation for the impact that it may have on a system.

Please take time to familarise yourself with the standard and configurable default values, and exclude any items before applying this role to a system.

An examples of items that should be immediately considered for exclusion (or at least, for modification of the related default values) include:

* 5.2.6`, '5.2.10', '5.2.20' which by default effectively limits or blocks access to the hosts or to localhost (including via ssh)


Example Playbook
----------------

An example playbook which uses this role is as follows:

```
---
- hosts: localhost
  connection: local
  gather_facts: true
  become: yes

  roles:
    - sles15
```

Note that the use of ```become: yes``` is required as 99% of tasks require privileged access to execute.


Role Variables
--------------
See ```defaults/main.yml``` for variables which can be overriden according to your preference and as per the environment. Check the inline comments in this variable file.


Options
-------
Tags (and combinations thereof) can be used to run a particular level of the CIS standard, a section, or an individual recommendation. For example:

* Run only Level 1 tasks

```
ansible-playbook deploy.yml -i inventory -t level-1
```


* Run tasks 1.3.1 and 2.2.10 only

```
ansible-playbook deploy.yml -i inventory -t 1.3.1,2.2.10
```

Limitations
-----------
At present, the following checks have not been implemented:

Section 1:
* Few control checks that are related to disk partitions : Partition configurations are server specific.
* Control checks related to patching: Patching is managed by Accenture for EON as a separate change activity

Section 3:
* Few control checks related to firewalls and iptables : As confirmed by E.ON Sec Arch team, these checks would be taken up in network segmentation project

Section 5:
* Few control checks related to SSH configurations causes connectivity issues and system becomes unreachable via ssh


Compatibility
-------------
This role is compatible with the following versions of :
* ansible: 2.9 or greater     
This role has not been tested against any other versions of ansible.

*Python: 3
This role has not been tested against any other versions of python.


Author Information
------------------

This role was developed by Parameshwari Konyala 

Reference: [Anth Courtney](https://github.com/anthcourtney/ansible-role-cis-amazon-linux) original github publication.
